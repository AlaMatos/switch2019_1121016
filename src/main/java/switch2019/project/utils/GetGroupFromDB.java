package switch2019.project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;

import java.util.Optional;

/**
 * @author Ala Matos
 */
@Component
public class GetGroupFromDB {

    @Autowired
    private IGroupRepository groupRepository;

    public final static String GROUP_DOES_NOT_EXIST = "Group doesn't exist";
    /**
     * Auxiliary method to get all persons groups from the DB
     *
     * @param groupDenomination
     * @return
     */
    public Group getGroupFromDB(String groupDenomination) {


//            ArrayList<Group> ListOfAllGroupsPersonIsMember = new ArrayList<>();
        GroupID groupIDToSearch = GroupID.createGroupID(groupDenomination);
//          Find all groups in the DB
        Optional<Group> optGroup = groupRepository.findById(groupIDToSearch);

        if(!optGroup.isPresent()){
            throw new NotFoundArgumentsBusinessException(GROUP_DOES_NOT_EXIST);
        }
        Group groupFromDB = optGroup.get();

        return groupFromDB;
    }
}

