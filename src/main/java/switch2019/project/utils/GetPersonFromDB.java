package switch2019.project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.util.Optional;

/**
 * @author Ala Matos
 */
@Component
public class GetPersonFromDB {

    @Autowired
    private IPersonRepository personRepository;

    /**
     * Auxiliary method to get all persons groups from the DB
     *
     * @param optPerson
     * @return
     */
    public Person getPersonFromDB(Optional<Person> optPerson) {


        Person personFromDB = optPerson.get();
        return personFromDB;

    }

}
