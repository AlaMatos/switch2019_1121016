package switch2019.project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;

/**
 * @author Ala Matos
 */
@Component
public class AnalyzeIfAdmin {

        @Autowired
        private IGroupRepository groupRepository;
        @Autowired
        private GetGroupFromDB getGroupFromDB;
        /**
         * The constant EMPTY_LEDGER.
         */
        public final static String PERSON_IS_NOT_ADMIN = "Person is not a admin of the given group";

        public AnalyzeIfAdmin(IGroupRepository groupRepository, GetGroupFromDB getGroupFromDB) {
            this.groupRepository= groupRepository;
            this.getGroupFromDB=getGroupFromDB;
        }

        /**
         * Auxiliary method to get all persons groups from the DB
         *
         * @param personEmail
         * @param groupDenomination
         * @return
         */
        public boolean analyzeIfAdmin(String personEmail, String groupDenomination) {

            AssemblePersonID assemblePersonID = new AssemblePersonID();
            PersonID personIDToSearch = assemblePersonID.assemblePersonID(personEmail);
            Group groupFromDB = getGroupFromDB.getGroupFromDB(groupDenomination);
            if (!groupFromDB.isPersonPeopleInCharge(personIDToSearch)) {
                throw new InvalidArgumentsBusinessException(PERSON_IS_NOT_ADMIN);
            }
            return true;
        }
    }




