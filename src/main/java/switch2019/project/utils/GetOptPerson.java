package switch2019.project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IPersonRepository;

import java.util.Optional;

/**
 * @author Ala Matos
 */

/**
 * Auxiliary method to get Person from the DB

 */
@Component
public class GetOptPerson {
    @Autowired
    private IPersonRepository personRepository;
@Autowired
    private AssemblePersonID assemblePersonID;
    /**
     * The constant PERSON_DOES_NOT_EXIST.
     */
    public final static String PERSON_DOES_NOT_EXISTS = "Person not registered in the system";


    public Optional<Person> getOptPerson(String personEmail) {

//        Create a PersonID from the info received from the DTO
        PersonID personIDToSearch = assemblePersonID.assemblePersonID(personEmail);

//        Get the person with the PersonID previously created
        Optional<Person> optPerson = personRepository.findById(personIDToSearch);

//        If in the DB doesn't exists the person searched, it must return an exception
        if (!optPerson.isPresent()) {
            throw new InvalidArgumentsBusinessException(PERSON_DOES_NOT_EXISTS);
        }
        return optPerson;
    }

}
