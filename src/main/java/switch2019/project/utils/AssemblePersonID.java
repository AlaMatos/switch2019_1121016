package switch2019.project.utils;

import org.springframework.stereotype.Component;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;

/**
 * @author Ala Matos
 */
@Component
public class AssemblePersonID {

    /**
     * Auxiliary method to create PersonID
     *
     * @param personEmail
     * @return
     */
    public PersonID assemblePersonID(String personEmail) {

        return PersonID.createPersonID(personEmail);
    }
}
