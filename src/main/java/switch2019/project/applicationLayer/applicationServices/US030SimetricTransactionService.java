package switch2019.project.applicationLayer.applicationServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Ledger;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.*;
import switch2019.project.dtoLayer.dtos.CreateGroupTransactionDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.utils.*;

import java.time.LocalDate;
import java.util.Optional;

/**
 * @author Ala Matos
 */
@Service
public class US030SimetricTransactionService {

    public final static String PERSON_NOT_IN_CHARGE = "Person is not in charge";
    /**
     * The constant GROUP_DOES_NOT_EXIST.
     */
    public final static String GROUP_DOES_NOT_EXIST = "Group does not exist in the system";
    /**
     * The constant PERSON_NOT_MEMBER.
     */
    public final static String PERSON_NOT_MEMBER = "Person is not member of the group";
    /**
     * The constant NEED_TO_CREATE_CATEGORY.
     */
    public final static String NEED_TO_CREATE_CATEGORY = "Category does not exist; it needs to be created";
    /**
     * The constant NEED_TO_CREATE_ACCOUNT_TO_CREDIT.
     */
    public final static String NEED_TO_CREATE_ACCOUNT_TO_CREDIT = "Account to be credited does not exist; it needs to be created";
    /**
     * The constant NEED_TO_CREATE_ACCOUNT_TO_DEBIT.
     */
    public final static String NEED_TO_CREATE_ACCOUNT_TO_DEBIT = "Account to be debited does not exist; it needs to be created";

    @Autowired
    private IPersonRepository personRepository;
    @Autowired
    private IGroupRepository groupRepository;
    @Autowired
    private ILedgerRepository ledgerRepository;
    @Autowired
    private ICategoryRepository categoryRepository;
    @Autowired
    private IAccountRepository accountRepository;

    private AssemblePersonID assemblePersonID;
    private AnalyzeIfAdmin analyzeIfAdmin;
    private GetPersonFromDB getPersonFromDB;
    private GetGroupFromDB getGroupFromDB;
    private GetOptPerson getOptPerson;

    public US030SimetricTransactionService(IPersonRepository personRepository, IGroupRepository groupRepository, ILedgerRepository ledgerRepository,
                                           ICategoryRepository categoryRepository, IAccountRepository accountRepository,
                                           AssemblePersonID assemblePersonID, AnalyzeIfAdmin analyzeIfAdmin,
                                           GetOptPerson getOptPerson, GetGroupFromDB getGroupFromDB, GetPersonFromDB getPersonFromDB) {
        this.personRepository = personRepository;
        this.groupRepository = groupRepository;
        this.categoryRepository= categoryRepository;
        this.accountRepository= accountRepository;
        this.ledgerRepository= ledgerRepository;
        this.assemblePersonID = assemblePersonID;
        this.analyzeIfAdmin = analyzeIfAdmin;
        this.getOptPerson = getOptPerson;
        this.getGroupFromDB = getGroupFromDB;
        this.getPersonFromDB = getPersonFromDB;
    }

    public GroupDTO createSimetricMovements(CreateGroupTransactionDTO createGroupTransactionDTO) {


//            Verify if person is member/admin of the given group
        String groupDenomination = createGroupTransactionDTO.getGroupDenomination();
        String personEmail = createGroupTransactionDTO.getPersonGroupMemberEmail();
        PersonID personID = assemblePersonID.assemblePersonID(createGroupTransactionDTO.getPersonGroupMemberEmail());

        boolean checkIfPersonIsAdminOfGroup = analyzeIfAdmin.analyzeIfAdmin(personEmail, groupDenomination);
        Optional<Person> optPerson = getOptPerson.getOptPerson(createGroupTransactionDTO.getPersonGroupMemberEmail());
        Person personFromDB = getPersonFromDB.getPersonFromDB(optPerson);
        Group groupFromDB = getGroupFromDB.getGroupFromDB(groupDenomination);

        GroupID groupID = groupFromDB.getGroupID();

        //If category does not exist, needs to be created
        CategoryID groupCategoryID = CategoryID.createCategoryID(createGroupTransactionDTO.getCategoryDenomination(), groupID);
        boolean categoryExistsInRepo = categoryRepository.existsById(groupCategoryID);

        //If category does not exist, needs to be created
        CategoryID personCategoryID = CategoryID.createCategoryID(createGroupTransactionDTO.getCategoryDenomination(), personID);
        boolean personCategoryExistsInRepo = categoryRepository.existsById(personCategoryID);

        //If account to be debited does not exist, needs to be created
        AccountID groupAccountToDebitID = AccountID.createAccountID(createGroupTransactionDTO.getAccountToDebitName(), groupID);
        boolean groupAccountToDebitExistsInRepo = accountRepository.existsById(groupAccountToDebitID);

        //If account to be debited does not exist, needs to be created
        AccountID personAccountToDebitID = AccountID.createAccountID(createGroupTransactionDTO.getAccountToDebitName(), groupID);
        boolean personAccountToDebitExistsInRepo = accountRepository.existsById(personAccountToDebitID);

        //If account to be credited does not exist, needs to be created
        AccountID groupAccountToCreditID = AccountID.createAccountID(createGroupTransactionDTO.getAccountToCreditName(), groupID);
        boolean groupAccountToCreditExistsInRepo = accountRepository.existsById(groupAccountToCreditID);

        //If account to be credited does not exist, needs to be created
        AccountID personAccountToCreditID = AccountID.createAccountID(createGroupTransactionDTO.getAccountToCreditName(), groupID);
        boolean personAccountToCreditExistsInRepo = accountRepository.existsById(personAccountToCreditID);

        //If ledger does not exist
        LedgerID groupLedgerID = groupFromDB.getLedgerID();
        Optional<Ledger> optGroupLedger = ledgerRepository.findById(groupLedgerID);

        //If ledger does not exist
        LedgerID ledgerID = personFromDB.getLedgerID();
        Optional<Ledger> optPersonLedger = ledgerRepository.findById(ledgerID);

        if (!checkIfPersonIsAdminOfGroup) {
            throw new InvalidArgumentsBusinessException(PERSON_NOT_IN_CHARGE);


        } else if (!(categoryExistsInRepo && personCategoryExistsInRepo)) {
            throw new NotFoundArgumentsBusinessException(NEED_TO_CREATE_CATEGORY);


        } else if (!(groupAccountToDebitExistsInRepo && personAccountToDebitExistsInRepo)) {
            throw new NotFoundArgumentsBusinessException(NEED_TO_CREATE_ACCOUNT_TO_DEBIT);


        } else if (!(groupAccountToCreditExistsInRepo && personAccountToCreditExistsInRepo)) {
            throw new NotFoundArgumentsBusinessException(NEED_TO_CREATE_ACCOUNT_TO_CREDIT);

        } else {
            LocalDate date = LocalDate.parse(createGroupTransactionDTO.getDate());

            //If all information required for creating a transaction is available, transaction can be created and added to the group's ledger
            Ledger personLedger = optPersonLedger.get();
            Ledger groupLedger = optGroupLedger.get();

            personLedger.createAndAddTransactionWithDate(personCategoryID, createGroupTransactionDTO.getTransactionType(),
                    createGroupTransactionDTO.getTransactionDescription(), createGroupTransactionDTO.getTransactionAmount(),
                    date, personAccountToDebitID, personAccountToCreditID);

            groupLedger.createAndAddTransactionWithDate(groupCategoryID, createGroupTransactionDTO.getTransactionType(),
                    createGroupTransactionDTO.getTransactionDescription(), createGroupTransactionDTO.getTransactionAmount(),
                    date, groupAccountToCreditID,groupAccountToDebitID);

            ledgerRepository.addAndSaveTransaction(personLedger);
            ledgerRepository.addAndSaveTransaction(groupLedger);


        }
        return GroupDTOAssembler.createDTOFromDomainObject(groupFromDB.getGroupID().getDenomination(), groupFromDB.getDescription(),
                groupFromDB.getDateOfCreation(), groupFromDB.getLedgerID());
    }
}


