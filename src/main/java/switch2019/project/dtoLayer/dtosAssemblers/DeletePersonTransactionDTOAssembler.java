package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.DeletePersonTransactionDTO;

public class DeletePersonTransactionDTOAssembler {

    private DeletePersonTransactionDTOAssembler() {
    }

    public static DeletePersonTransactionDTO createDTOFromPrimitiveTypes(int transactionNumber, String email) {
        DeletePersonTransactionDTO deletePersonTransactionDTO = new DeletePersonTransactionDTO(transactionNumber, email);
        return deletePersonTransactionDTO;

    }

}
