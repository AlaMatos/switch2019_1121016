package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.SearchTransactionFromDomainDTO;
import switch2019.project.dtoLayer.dtos.TransactionWithOwnerInfoFromDomainDTO;

import java.util.List;

/**
 * @author Ala Matos
 */
public class SearchTransactionFromDomainDTOAssembler {

    /**
     * Instantiates a new SearchTransactionFromDomainDTO
     * @param transactions
     * @return
     */
    public static SearchTransactionFromDomainDTO createDTOFromDomainType(List<TransactionWithOwnerInfoFromDomainDTO> transactions) {

        SearchTransactionFromDomainDTO searchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();

//          Loop through all the transaction within the list received as parameter,
//          adding each one to the searchTransactionFromDomainDTO
        for (TransactionWithOwnerInfoFromDomainDTO transaction : transactions) {
            searchTransactionFromDomainDTO.getTransactions().add(transaction);
        }

        return searchTransactionFromDomainDTO;
    }
}
