package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US030SimetricTransactionService;
import switch2019.project.dtoLayer.dtos.CreateGroupTransactionDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupTransactionDTOAssembler;

/**
 * @author Ala Matos
 */
@Controller
public class US030SimetricTransactionController {

        @Autowired
        private US030SimetricTransactionService us030Service;

        //      Como administrador de 2 livros-razão, quero criar um movimento num livro razão
        //que seja o simétrico de determinado movimento de outro livro-razão (i.e. crédito <->
        //débito, conta de débito <-> conta de crédito).

        public US030SimetricTransactionController(US030SimetricTransactionService us030Service) {
            this.us030Service = us030Service;
        }

    public GroupDTO createGroupTransaction(String groupDenomination, String personGroupMemberEmail, String categoryDenomination, String accountToDebitName,
                                           String accountToCreditName, double transactionAmount, String transactionType, String transactionDescription, String date) {

        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination,
                personGroupMemberEmail, categoryDenomination, accountToDebitName, accountToCreditName, transactionAmount, transactionType, transactionDescription, date);

        return us030Service.createSimetricMovements(createGroupTransactionDTO);
    }

//        public SearchTransactionFromDomainDTO getRecordsBetweenTwoDates(String personID, String startDate, String endDate) {
//
//            SearchTransactionDTO searchTransactionDTO = SearchTransactionDTOAssembler.createDTOFromPrimitiveType(personID,
//                    startDate, endDate);
//
//            return us030Service.getAllPersonsTransactions(searchTransactionDTO);
//
//        }

    }
