package switch2019.project.controllerLayer.controllers.controllersCLI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import switch2019.project.applicationLayer.applicationServices.US007CreateGroupAccountService;
import switch2019.project.dtoLayer.dtos.CreateGroupAccountDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupAccountDTOAssembler;

/* US007 Como responsável do grupo, quero criar uma conta do grupo,
   atribuindo-lhe uma denominação e uma descrição, para posteriormente poder ser usada nos movimentos do grupo.
*/

/**
 * The type Us 007 create group account controller.
 */
@Controller
public class US007CreateGroupAccountController {

    @Autowired
    private US007CreateGroupAccountService us007CreateGroupAccountService;

    /**
     * Instantiates a new Us 007 create group account controller.
     *
     * @param us007CreateGroupAccountService the us 007 create group account service
     */
    public US007CreateGroupAccountController(US007CreateGroupAccountService us007CreateGroupAccountService) {
        this.us007CreateGroupAccountService = us007CreateGroupAccountService;
    }

    /**
     * Create account as people in charge boolean dto.
     *
     * @param personEmail         the person email
     * @param groupDenomination   the group denomination
     * @param accountDescription  the account description
     * @param accountDenomination the account denomination
     * @return the boolean dto
     */
    public GroupDTO createAccountAsPeopleInCharge(String personEmail, String groupDenomination, String accountDescription, String accountDenomination) {
        CreateGroupAccountDTO createGroupAccountDTO = CreateGroupAccountDTOAssembler.createDTOFromPrimitiveTypes(
                personEmail, groupDenomination, accountDescription, accountDenomination);

        return us007CreateGroupAccountService.createAccountAsPeopleInCharge(createGroupAccountDTO);
    }


}


