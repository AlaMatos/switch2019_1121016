package switch2019.project.controllerLayer.controllers.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import switch2019.project.applicationLayer.applicationServices.US030SimetricTransactionService;
import switch2019.project.controllerLayer.controllers.controllersCLI.US030SimetricTransactionController;
import switch2019.project.dtoLayer.dtos.CreateGroupTransactionDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtos.NewGroupCategoryInfoDTO;
import switch2019.project.dtoLayer.dtos.NewGroupTransactionInfoDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupTransactionDTOAssembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * @author Ala Matos
 */
@RestController
public class US030SimetricTransactionControllerRest {

        @Autowired
        private US030SimetricTransactionService us030Service;

        public US030SimetricTransactionControllerRest(US030SimetricTransactionService us030Service) {
            this.us030Service = us030Service;
        }


        @PostMapping("/persons/{personEmail}/groups/{groupDenomination}/ledgers/records/US030")
        public ResponseEntity<Object> simetricTransaction(@RequestBody NewGroupTransactionInfoDTO info,
                                                               @PathVariable final String personEmail,
                                                               @PathVariable final String groupDenomination) {

//DTO for passing info to service that searches account records within period
            CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(groupDenomination,
                    personEmail, info.getDenominationCategory(), info.getDenominationAccountDeb(), info.getDenominationAccountCred(), info.getAmount(),
                    info.getType(), info.getDescription(), info.getDate());
            //Info to return considering if form is empty or not

            GroupDTO groupDTO = us030Service.createSimetricMovements(createGroupTransactionDTO);

            Link link_to_admins = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAdmins(groupDenomination)).withRel("admins");
            Link link_to_members = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupMembers(groupDenomination)).withRel("members");
            Link link_to_ledger = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupLedger(groupDenomination)).withRel("ledger");
            Link link_to_accounts = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupAccounts(personEmail, groupDenomination)).withRel("accounts");
            Link link_to_categories = linkTo(methodOn(US002_1CreateGroupControllerREST.class).getGroupCategories(personEmail, groupDenomination)).withRel("categories");

            groupDTO.add(link_to_admins);
            groupDTO.add(link_to_members);
            groupDTO.add(link_to_ledger);
            groupDTO.add(link_to_accounts);
            groupDTO.add(link_to_categories);

            return new ResponseEntity<>(groupDTO, HttpStatus.CREATED);
        }
    }

