package switch2019.project.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Ledger;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Address;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.domainEntities.vosShared.LedgerID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Ala Matos
 */
class AnalyzeIfAdminTest extends AbstractTest{

        @Mock
        private IGroupRepository groupRepository;
        @Mock
        private GetGroupFromDB getGroupFromDB;

        private AnalyzeIfAdmin analyzeIfAdmin;

        //    Parameters of person
        private Person personPaulo;
        private Person personRita;
        private Person personHenrique;
        private Person personPaulinho;
        private String pauloEmail;
        private PersonID pauloPersonID;
        private Ledger ledger;
        private LedgerID ledgerID;

        //    Parameters of a admin
//        Rita
        private String ritaEmail;
        private String henriqueEmail;
        private String paulinhoEmail;
        private PersonID ritaPersonID;
        private PersonID henriquePersonID;
        private PersonID paulinhoPersonID;

        //    Parameters of group sundayRunners
        private Group sundayRunners;
        private GroupID sundayRunnersID;
        private Ledger sundayRunnersLedger;
        private LedgerID sundayRunnersLedgerID;


        @BeforeEach
        public void init() {

//        Ledger
            ledger = Ledger.createLedger();
            ledgerID = ledger.getLedgerID();

//        Person Paulo

//        Address
            String portoStreet = "Rua Direita do Viso";
            String portoDoorNumber = "59";
            String portoPostCode = "4250 - 198";
            String portoCity = "Porto";
            String portoCountry = "Portugal";

            Address porto = Address.createAddress(portoStreet, portoDoorNumber, portoPostCode, portoCity, portoCountry);

//        Information to instantiate a Person Paulo
            pauloEmail = "paulo@gmail.com";
            String pauloName = "Paulo Fontes";
            LocalDate pauloBirthdate = LocalDate.of(1993, 03, 15);
            String pauloBirthplace = "Vila Nova de Gaia";

            pauloPersonID = PersonID.createPersonID(pauloEmail);

            personPaulo = Person.createPersonWithoutParents(pauloEmail, pauloName, pauloBirthdate, pauloBirthplace, porto, ledgerID);

//        Information to instantiate a Person Rita
            ritaEmail = "rita@gmail.com";
            String ritaName = "Rita";
            LocalDate ritaBirthdate = LocalDate.of(1993, 03, 15);
            String ritaBirthplace = "Vila Nova de Gaia";

            ritaPersonID = PersonID.createPersonID(ritaEmail);

            personRita = Person.createPersonWithoutParents(ritaEmail, ritaName, ritaBirthdate, ritaBirthplace, porto, ledgerID);

            //        Information to instantiate a Person Paulo
            henriqueEmail = "henrique@gmail.com";
            String henriqueName = "Henique";
            LocalDate henriqueBirthdate = LocalDate.of(1993, 03, 15);
            String henriqueBirthplace = "Vila Nova de Gaia";

            henriquePersonID = PersonID.createPersonID(henriqueEmail);

            personHenrique = Person.createPersonWithoutParents(henriqueEmail, henriqueName, henriqueBirthdate, henriqueBirthplace, porto, ledgerID);

            //        Information to instantiate a Person Paulo
            paulinhoEmail = "paulinho@gmail.com";
            String paulinhoName = "Paulinho";
            LocalDate paulinhoBirthdate = LocalDate.of(1993, 03, 15);
            String paulinhoBirthplace = "Vila Nova de Gaia";

            paulinhoPersonID = PersonID.createPersonID(paulinhoEmail);

            personPaulinho = Person.createPersonWithoutParents(paulinhoEmail, paulinhoName, paulinhoBirthdate, paulinhoBirthplace, porto, ledgerID);


            //Sunday Runners
            //Accounts ->            Company / Bank Account
            //Categories ->          Salary

//        Ledger
            sundayRunnersLedger = Ledger.createLedger();
            sundayRunnersLedgerID = sundayRunnersLedger.getLedgerID();

            LocalDate dateOfCreation = LocalDate.of(2020, 6, 1);
            String sundayRunnersDenomination = "Sunday Runners";
            String sundayRunnersDescription = "All members from Sunday Runners group";

//        Information to instantiate a Group Sunday Runners
            sundayRunners = Group.createGroupAsPersonInCharge(sundayRunnersDenomination, pauloPersonID, sundayRunnersDescription, dateOfCreation, sundayRunnersLedgerID);
            sundayRunnersID = GroupID.createGroupID(sundayRunnersDenomination);

//        Add members
            sundayRunners.addMember(ritaPersonID);
            sundayRunners.addMember(henriquePersonID);
            sundayRunners.addMember(paulinhoPersonID);
        }

        @Test
        @DisplayName("Test For AnalyzeIfAdminTest()")
        void AnalyzeIfAdminTest() {
            analyzeIfAdmin = new AnalyzeIfAdmin(groupRepository, getGroupFromDB);
            // Arrange

            String personEmail = "paulo@gmail.com";
            String groupDenomination = "Sunday Runners";
//
//        //          Returns an Optional<Group>
//
            Mockito.when(getGroupFromDB.getGroupFromDB(groupDenomination))
                    .thenReturn(sundayRunners);
            Mockito.when(groupRepository.findById(sundayRunnersID)).thenReturn(Optional.of(sundayRunners));

            // Act

//        AnalyzeIfAdmin analyzeIfAdmin = new AnalyzeIfAdmin();
            boolean analysis = analyzeIfAdmin.analyzeIfAdmin(personEmail, groupDenomination);

            // Assert

            assertTrue(analysis);
        }

    }

