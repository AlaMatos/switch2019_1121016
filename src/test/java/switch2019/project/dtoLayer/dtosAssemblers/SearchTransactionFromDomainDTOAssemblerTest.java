package switch2019.project.dtoLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.dtoLayer.dtos.SearchTransactionFromDomainDTO;
import switch2019.project.dtoLayer.dtos.TransactionWithOwnerInfoFromDomainDTO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Ala Matos
 */
class SearchTransactionFromDomainDTOAssemblerTest {

    @Test
    @DisplayName("Test the constructor")
    void testConstructor() {

//        Arrange first transaction
        String name = "Hulk";
        String hulkEmail = "hulk@gmail.com";
        String category = "grocery";
        String type = "debit";
        String description = "month groceries";
        double amount = 50;
        String date = "2020-06-22";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateLD = LocalDate.parse(date, formatter);
        String debitAccount = "wallet";
        String creditAccount = "SuperMarket";

//        Arrange second transaction
        String secondCategory = "monthly bills";
        String secondType = "debit";
        String secondDescription = "Water bill";
        double secondAmount = 25;
        String secondDate = "2020-06-12";

        LocalDate secondDateLD = LocalDate.parse(secondDate, formatter);
        String secondDebitAccount = "wallet";
        String secondCreditAccount = "water bill";

//          Arrange PersonID
        PersonID hulkID = PersonID.createPersonID(hulkEmail);

//          Arrange FirstTransaction
        CategoryID firstCategoryID = CategoryID.createCategoryID(category, hulkID);
        AccountID firstDebitAccountID = AccountID.createAccountID(debitAccount, hulkID);
        AccountID firstCreditAccountID = AccountID.createAccountID(creditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID secondCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID secondDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID secondCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);

//        Act  -> Create the transactions to be added as parameter in the creation of the TransactionWithOwnerInfoFromDomainDTO
        Transaction firstTransaction = Transaction.createTransaction(firstCategoryID, type, description, amount, dateLD, firstDebitAccountID, firstCreditAccountID);
        Transaction secondTransaction = Transaction.createTransaction(secondCategoryID, secondType, secondDescription, secondAmount, secondDateLD,
                secondDebitAccountID, secondCreditAccountID);

//        Act  -> Create a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(firstTransaction, name);

//        Act  -> Create another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(secondTransaction, name);

//        Act  -> Create the List of TransactionWithOwnerInfoFromDomainDTO
        List<TransactionWithOwnerInfoFromDomainDTO> transactions = new ArrayList<>();
        transactions.add(firstTransactionAdded);
        transactions.add(secondTransactionAdded);

//        Act  -> Create the actual SearchTransactionFromDomainDTO using the assembler
        SearchTransactionFromDomainDTO actualSearchTransactionFromDomainDTO = SearchTransactionFromDomainDTOAssembler.createDTOFromDomainType(transactions);

//        Act  -> Create the expected SearchTransactionFromDomainDTO
        SearchTransactionFromDomainDTO expectedListSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> listOfTransactions = expectedListSearchTransactionFromDomainDTO.getTransactions();
        listOfTransactions.add(firstTransactionAdded);
        listOfTransactions.add(secondTransactionAdded);

//        Assert
        assertEquals(expectedListSearchTransactionFromDomainDTO, actualSearchTransactionFromDomainDTO);

    }
}