package switch2019.project.dtoLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.dtoLayer.dtos.TransactionWithOwnerInfoFromDomainDTO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Ala Matos
 */
class TransactionWithOwnerInfoFromDomainDTOAssemblerTest {

    @Test
    @DisplayName("Test constructor")
    void testConstructor() {

//        Arrange the needed info to create a Transaction
//            Person/PersonID
        String personName = "Hulk";
        String hulkEmail = "hulk@gmail.com";
        PersonID hulkID = PersonID.createPersonID(hulkEmail);
//            Category
        String category = "grocery";
        CategoryID hulkCategoryID = CategoryID.createCategoryID(category, hulkID);
//            Type
        String type = "debit";
//            Description of the transaction
        String description = "month groceries";
//            Amount of the transaction
        double amount = 50;
//            Date of the transaction
        String date= "2020-06-22";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateLD = LocalDate.parse(date, formatter);
//            Debit Account
        String debitAccount= "wallet";
        AccountID hulkDebitAccountID = AccountID.createAccountID(debitAccount, hulkID);
//            Credit Account
        String creditAccount="SuperMarket";
        AccountID hulkCreditAccountID = AccountID.createAccountID(creditAccount, hulkID);

//        Act
//            Create a transaction
        Transaction transaction = Transaction.createTransaction(hulkCategoryID, type, description, amount, dateLD, hulkDebitAccountID, hulkCreditAccountID);

//            Create the expected TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO expectedTransaction = new TransactionWithOwnerInfoFromDomainDTO(personName, category, type,
                description, amount, date, debitAccount, creditAccount);

//            Create a TransactionWithOwnerInfoFromDomainDTO through its assembler
        TransactionWithOwnerInfoFromDomainDTO actualTransaction = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(transaction, personName);

//        Assert
        assertEquals(expectedTransaction.getOwnerInfo(),actualTransaction.getOwnerInfo());
        assertEquals(expectedTransaction.getCategory(),actualTransaction.getCategory());
        assertEquals(expectedTransaction.getType(),actualTransaction.getType());
        assertEquals(expectedTransaction.getDescription(),actualTransaction.getDescription());
        assertEquals(expectedTransaction.getAmount(),actualTransaction.getAmount());
        assertEquals(expectedTransaction.getDate(),actualTransaction.getDate());
        assertEquals(expectedTransaction.getDebitAccount(),actualTransaction.getDebitAccount());
        assertEquals(expectedTransaction.getCreditAccount(),actualTransaction.getCreditAccount());
    }

}