package switch2019.project.dtoLayer.dtosAssemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.dtoLayer.dtos.GroupAllMembersDTO;
import switch2019.project.dtoLayer.dtos.GroupMemberClearanceDTO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GroupAllMembersDTOAssemblerTest {

    @Test
    @DisplayName("GroupAllMembersDTOAssembler - Test create GroupListDTO from domain objects")
    void groupAdminsDTOAssembler_createDTOFromDomainObject() {

        //Arrange

        String manuelEmail = "manuel@gmail.com";
        String manuelClearance = "Member";

        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(manuelEmail, manuelClearance);

        List<GroupMemberClearanceDTO> groupMemberClearanceDTOList = new ArrayList<>();
        groupMemberClearanceDTOList.add(groupMemberClearanceDTO);

        //Act

        GroupAllMembersDTOAssembler groupAllMembersDTOAssembler = new GroupAllMembersDTOAssembler();
        GroupAllMembersDTO groupAllMembersDTO = groupAllMembersDTOAssembler.createDTOFromDomainObject(groupMemberClearanceDTOList);

        //Assert
        assertEquals(groupMemberClearanceDTOList, groupAllMembersDTO.getAllMembers());
    }
}