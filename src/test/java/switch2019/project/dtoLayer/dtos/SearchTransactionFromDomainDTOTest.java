package switch2019.project.dtoLayer.dtos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.vosShared.AccountID;
import switch2019.project.domainLayer.domainEntities.vosShared.CategoryID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.dtoLayer.dtosAssemblers.TransactionWithOwnerInfoFromDomainDTOAssembler;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * @author Ala Matos
 */
class SearchTransactionFromDomainDTOTest {

    //Parameters of the firstTransaction
    private String hulkName;
    private String hulkEmail;
    private String firstCategory;
    private String firstType;
    private String firstDescription;
    private double firstAmount;
    private String firstDate;
    private LocalDate firstDateLD;
    private String firstDebitAccount;
    private String firstCreditAccount;


    //Parameters of the secondTransaction
    private String secondCategory;
    private String secondType;
    private String secondDescription;
    private double secondAmount;
    private String secondDate;
    private LocalDate secondDateLD;
    private String secondDebitAccount;
    private String secondCreditAccount;

    @BeforeEach
    void init() {

//        Arrange first transaction
        hulkName = "Hulk";
        hulkEmail = "hulk@gmail.com";
        firstCategory = "grocery";
        firstType = "debit";
        firstDescription = "month groceries";
        firstAmount = 50;
        firstDate = "2020-06-22";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        firstDateLD = LocalDate.parse(firstDate, formatter);
        firstDebitAccount = "wallet";
        firstCreditAccount = "SuperMarket";

//        Arrange second transaction
        secondCategory = "monthly bills";
        secondType = "debit";
        secondDescription = "Water bill";
        secondAmount = 25;
        secondDate = "2020-06-12";

        secondDateLD = LocalDate.parse(secondDate, formatter);
        secondDebitAccount = "wallet";
        secondCreditAccount = "water bill";

    }

    @Test
    @DisplayName("Test the constructor")
    void testConstructorUsingGetMethod() {

//          Arrange PersonID
        PersonID hulkID = PersonID.createPersonID(hulkEmail);

//          Arrange FirstTransaction
        CategoryID firstCategoryID = CategoryID.createCategoryID(firstCategory, hulkID);
        AccountID firstDebitAccountID = AccountID.createAccountID(firstDebitAccount, hulkID);
        AccountID firstCreditAccountID = AccountID.createAccountID(firstCreditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID secondCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID secondDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID secondCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);


//        Act  -> Create the transactions to be added as parameter in the creation of the TransactionWithOwnerInfoFromDomainDTO
        Transaction firstTransaction = Transaction.createTransaction(firstCategoryID, firstType, firstDescription, firstAmount,
                firstDateLD, firstDebitAccountID, firstCreditAccountID);

        Transaction secondTransaction = Transaction.createTransaction(secondCategoryID, secondType, secondDescription, secondAmount,
                secondDateLD, secondDebitAccountID, secondCreditAccountID);

//        Act  -> Create the TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(firstTransaction, hulkName);

        TransactionWithOwnerInfoFromDomainDTO secondTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(secondTransaction, hulkName);

//        Act  -> Create the SearchTransactionFromDomainDTO
        SearchTransactionFromDomainDTO searchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> listOfTransactions = searchTransactionFromDomainDTO.getTransactions();
        listOfTransactions.add(firstTransactionAdded);
        listOfTransactions.add(secondTransactionAdded);

//        Act  -> Create the expectedList of TransactionWithOwnerInfoFromDomainDTO
        List<TransactionWithOwnerInfoFromDomainDTO> expectedList = new ArrayList<>();
        expectedList.add(firstTransactionAdded);
        expectedList.add(secondTransactionAdded);


//        Assert
        assertEquals(expectedList, searchTransactionFromDomainDTO.getTransactions());
    }

    @Test
    @DisplayName("Test the set method")
    void testSetMethod() {

//          Arrange PersonID
        PersonID hulkID = PersonID.createPersonID(hulkEmail);

//          Arrange FirstTransaction
        CategoryID firstCategoryID = CategoryID.createCategoryID(firstCategory, hulkID);
        AccountID firstDebitAccountID = AccountID.createAccountID(firstDebitAccount, hulkID);
        AccountID firstCreditAccountID = AccountID.createAccountID(firstCreditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID secondCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID secondDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID secondCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);


//        Act  -> Create the transactions to be added as parameter in the creation of the TransactionWithOwnerInfoFromDomainDTO
        Transaction firstTransaction = Transaction.createTransaction(firstCategoryID, firstType, firstDescription, firstAmount,
                firstDateLD, firstDebitAccountID, firstCreditAccountID);

        Transaction secondTransaction = Transaction.createTransaction(secondCategoryID, secondType, secondDescription, secondAmount,
                secondDateLD, secondDebitAccountID, secondCreditAccountID);

//        Act  -> Create a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(firstTransaction, hulkName);

//        Act  -> Create another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(secondTransaction, hulkName);

//        Act  -> Create the SearchTransactionFromDomainDTO
        SearchTransactionFromDomainDTO firstSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> listOfTransactions = firstSearchTransactionFromDomainDTO.getTransactions();
        listOfTransactions.add(firstTransactionAdded);
        listOfTransactions.add(secondTransactionAdded);

//        Act  -> Create the expectedList of TransactionWithOwnerInfoFromDomainDTO
        SearchTransactionFromDomainDTO secondSearchTransactionFromDomainDTOs = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> expectedList = new ArrayList<>();
        expectedList.add(firstTransactionAdded);
        expectedList.add(secondTransactionAdded);

//        Set the List<TransactionWithOwnerInfoFromDomainDTO> of the secondSearchTransactionFromDomainDTOs
        secondSearchTransactionFromDomainDTOs.setTransactions(expectedList);

//        Assert
        assertEquals(firstSearchTransactionFromDomainDTO, secondSearchTransactionFromDomainDTOs);
    }

    @Test
    @DisplayName("Test the equals method | Two equals SearchTransactionFromDomainDTO")
    void testEqualsMethod() {

//          Arrange PersonID
        PersonID hulkID = PersonID.createPersonID(hulkEmail);

//          Arrange FirstTransaction
        CategoryID firstCategoryID = CategoryID.createCategoryID(firstCategory, hulkID);
        AccountID firstDebitAccountID = AccountID.createAccountID(firstDebitAccount, hulkID);
        AccountID firstCreditAccountID = AccountID.createAccountID(firstCreditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID secondCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID secondDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID secondCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);

//        Act  -> Create the transactions to be added as parameter in the creation of the TransactionWithOwnerInfoFromDomainDTO
        Transaction firstTransaction = Transaction.createTransaction(firstCategoryID, firstType, firstDescription, firstAmount,
                firstDateLD, firstDebitAccountID, firstCreditAccountID);

        Transaction secondTransaction = Transaction.createTransaction(secondCategoryID, secondType, secondDescription, secondAmount,
                secondDateLD, secondDebitAccountID, secondCreditAccountID);

//        Act  -> Create a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(firstTransaction, hulkName);
//        Act  -> Create another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(secondTransaction, hulkName);

//        Act  -> Create the SearchTransactionFromDomainDTO
        SearchTransactionFromDomainDTO firstSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> firstListOfTransactions = firstSearchTransactionFromDomainDTO.getTransactions();
        firstListOfTransactions.add(firstTransactionAdded);
        firstListOfTransactions.add(secondTransactionAdded);

//        Act  -> Create the expectedList of TransactionWithOwnerInfoFromDomainDTO
        SearchTransactionFromDomainDTO secondSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> secondListOfTransactions = secondSearchTransactionFromDomainDTO.getTransactions();
        secondListOfTransactions.add(firstTransactionAdded);
        secondListOfTransactions.add(secondTransactionAdded);

//        Assert
        assertEquals(firstSearchTransactionFromDomainDTO, secondSearchTransactionFromDomainDTO);
    }

    @Test
    @DisplayName("Test the equals method | Two different SearchTransactionFromDomainDTO")
    void testEqualsMethodDifferentSearchTransactionFromDomainDTO() {

//          Arrange PersonID
        PersonID hulkID = PersonID.createPersonID(hulkEmail);

//          Arrange FirstTransaction
        CategoryID firstCategoryID = CategoryID.createCategoryID(firstCategory, hulkID);
        AccountID firstDebitAccountID = AccountID.createAccountID(firstDebitAccount, hulkID);
        AccountID firstCreditAccountID = AccountID.createAccountID(firstCreditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID secondCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID secondDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID secondCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID thirdCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID thirdDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID thirdCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);

//        Act  -> Create the transactions to be added as parameter in the creation of the TransactionWithOwnerInfoFromDomainDTO
        Transaction firstTransaction = Transaction.createTransaction(firstCategoryID, firstType, firstDescription, firstAmount,
                firstDateLD, firstDebitAccountID, firstCreditAccountID);

        Transaction secondTransaction = Transaction.createTransaction(secondCategoryID, secondType, secondDescription, secondAmount,
                secondDateLD, secondDebitAccountID, secondCreditAccountID);

        Transaction thirdTransaction = Transaction.createTransaction(thirdCategoryID, secondType, secondDescription, secondAmount,
                secondDateLD, thirdDebitAccountID, thirdCreditAccountID);

//        Act  -> Create a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(firstTransaction, hulkName);

//        Act  -> Create another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(secondTransaction, hulkName);

//        Act  -> Create a third TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO thirdTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(thirdTransaction, hulkName);

//        Act  -> Create the SearchTransactionFromDomainDTO
        SearchTransactionFromDomainDTO firstSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> firstListOfTransactions = firstSearchTransactionFromDomainDTO.getTransactions();
        firstListOfTransactions.add(firstTransactionAdded);
        firstListOfTransactions.add(secondTransactionAdded);

//        Act  -> Create the expectedList of TransactionWithOwnerInfoFromDomainDTO
        SearchTransactionFromDomainDTO secondSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> secondListOfTransactions = secondSearchTransactionFromDomainDTO.getTransactions();
        secondListOfTransactions.add(secondTransactionAdded);
        secondListOfTransactions.add(thirdTransactionAdded);

//        Assert
        assertNotEquals(firstSearchTransactionFromDomainDTO, secondSearchTransactionFromDomainDTO);
    }

    @Test
    @DisplayName("Test the hashcode method | Two equals SearchTransactionFromDomainDTO")
    void testHashCodeMethod() {

//          Arrange PersonID
        PersonID hulkID = PersonID.createPersonID(hulkEmail);

//          Arrange FirstTransaction
        CategoryID firstCategoryID = CategoryID.createCategoryID(firstCategory, hulkID);
        AccountID firstDebitAccountID = AccountID.createAccountID(firstDebitAccount, hulkID);
        AccountID firstCreditAccountID = AccountID.createAccountID(firstCreditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID secondCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID secondDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID secondCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);

//        Act  -> Create the transactions to be added as parameter in the creation of the TransactionWithOwnerInfoFromDomainDTO
        Transaction firstTransaction = Transaction.createTransaction(firstCategoryID, firstType, firstDescription, firstAmount,
                firstDateLD, firstDebitAccountID, firstCreditAccountID);

        Transaction secondTransaction = Transaction.createTransaction(secondCategoryID, secondType, secondDescription, secondAmount,
                secondDateLD, secondDebitAccountID, secondCreditAccountID);

//        Act  -> Create the TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(firstTransaction, hulkName);

//        Act  -> Create another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(secondTransaction, hulkName);

//        Act  -> Create the SearchTransactionFromDomainDTO
        SearchTransactionFromDomainDTO firstSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> firstListOfTransactions = firstSearchTransactionFromDomainDTO.getTransactions();
        firstListOfTransactions.add(firstTransactionAdded);
        firstListOfTransactions.add(secondTransactionAdded);

//        Act  -> Create the expectedList of TransactionWithOwnerInfoFromDomainDTO
        SearchTransactionFromDomainDTO secondSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> secondListOfTransactions = secondSearchTransactionFromDomainDTO.getTransactions();
        secondListOfTransactions.add(firstTransactionAdded);
        secondListOfTransactions.add(secondTransactionAdded);

//        Assert
        assertEquals(firstSearchTransactionFromDomainDTO.hashCode(), secondSearchTransactionFromDomainDTO.hashCode());
    }

    @Test
    @DisplayName("Test the hashCode method | Two different SearchTransactionFromDomainDTO")
    void testHashCodeMethodDifferentSearchTransactionFromDomainDTO() {

//          Arrange PersonID
        PersonID hulkID = PersonID.createPersonID(hulkEmail);

//          Arrange FirstTransaction
        CategoryID firstCategoryID = CategoryID.createCategoryID(firstCategory, hulkID);
        AccountID firstDebitAccountID = AccountID.createAccountID(firstDebitAccount, hulkID);
        AccountID firstCreditAccountID = AccountID.createAccountID(firstCreditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID secondCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID secondDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID secondCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);

//          Arrange SecondTransaction
        CategoryID thirdCategoryID = CategoryID.createCategoryID(secondCategory, hulkID);
        AccountID thirdDebitAccountID = AccountID.createAccountID(secondDebitAccount, hulkID);
        AccountID thirdCreditAccountID = AccountID.createAccountID(secondCreditAccount, hulkID);

//        Act  -> Create the transactions to be added as parameter in the creation of the TransactionWithOwnerInfoFromDomainDTO
        Transaction firstTransaction = Transaction.createTransaction(firstCategoryID, firstType, firstDescription, firstAmount,
                firstDateLD, firstDebitAccountID, firstCreditAccountID);

        Transaction secondTransaction = Transaction.createTransaction(secondCategoryID, secondType, secondDescription, secondAmount,
                secondDateLD, secondDebitAccountID, secondCreditAccountID);

        Transaction thirdTransaction = Transaction.createTransaction(thirdCategoryID, secondType, secondDescription, secondAmount,
                secondDateLD, thirdDebitAccountID, thirdCreditAccountID);

//        Act  -> Create a TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO firstTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(firstTransaction, hulkName);

//        Act  -> Create another TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO secondTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(secondTransaction, hulkName);

//        Act  -> Create a third TransactionWithOwnerInfoFromDomainDTO
        TransactionWithOwnerInfoFromDomainDTO thirdTransactionAdded = TransactionWithOwnerInfoFromDomainDTOAssembler.createTransaction(secondTransaction, hulkName);

//        Act  -> Create the SearchTransactionFromDomainDTO
        SearchTransactionFromDomainDTO firstSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> firstListOfTransactions = firstSearchTransactionFromDomainDTO.getTransactions();
        firstListOfTransactions.add(firstTransactionAdded);
        firstListOfTransactions.add(secondTransactionAdded);

//        Act  -> Create the expectedList of TransactionWithOwnerInfoFromDomainDTO
        SearchTransactionFromDomainDTO secondSearchTransactionFromDomainDTO = new SearchTransactionFromDomainDTO();
        List<TransactionWithOwnerInfoFromDomainDTO> secondListOfTransactions = secondSearchTransactionFromDomainDTO.getTransactions();
        secondListOfTransactions.add(secondTransactionAdded);
        secondListOfTransactions.add(thirdTransactionAdded);

//        Assert
        assertNotEquals(firstSearchTransactionFromDomainDTO.hashCode(), secondSearchTransactionFromDomainDTO.hashCode());
    }

}