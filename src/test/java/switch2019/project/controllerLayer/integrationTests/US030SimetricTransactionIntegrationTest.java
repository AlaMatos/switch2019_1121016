package switch2019.project.controllerLayer.integrationTests;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import switch2019.project.dtoLayer.dtos.NewGroupCategoryInfoDTO;
import switch2019.project.dtoLayer.dtos.NewGroupTransactionInfoDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Ala Matos
 */

@TestInstance(TestInstance.Lifecycle.PER_CLASS)

@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
//----------------------------------------------------------------------------------------------------------------

//Adding @TestInstance(TestInstance.Lifecycle.PER_CLASS) to your test class will avoid that a
// new instance of your class is created for every test in the class. This is particularly useful
// when you have a lot of tests in the same test class and the instantiation of this class is expensive.
//If you would prefer that JUnit Jupiter execute all test methods on the same test instance, annotate
// your test class with @TestInstance(Lifecycle.PER_CLASS). When using this mode, a new test instance
// will be created once per test class.

//----------------------------------------------------------------------------------------------------------------
public class US030SimetricTransactionIntegrationTest extends AbstractTest {
    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }
    //The super keyword refers to superclass (parent) objects.
    //It is used to call superclass methods, and to access the superclass constructor.

    @Test
    @DisplayName("Test postMapping | Happy Path")
    void postSimetricMovement() throws Exception {

//      Arrange
//        Person email to add to the URI
        final String personEmail = "paulo@gmail.com";
//        Group email to add to the URI
        final String groupDenomination = "Sunday Runners";

//        Arrange transaction
        String denominationCategory = "Salary";
        String type = "debit";
        String transactionDescription = "first test";
        double amount = 50;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "Company";
        String date = "2020-07-06";

//        Uri
        final String uri = "/persons/" + personEmail + "/groups/"+groupDenomination+"/ledgers/records/US030/";

//          Input Json
        NewGroupTransactionInfoDTO newGroupTransactionInfoDTO = new NewGroupTransactionInfoDTO(denominationCategory, type,
                transactionDescription, amount, denominationAccountDeb, denominationAccountCred, date);
        String inputJson = super.mapToJson(newGroupTransactionInfoDTO);

//        Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();

//        Get the HTTP response
        final int status = mvcResult.getResponse().getStatus();

//        Assign the response content to a JSON Object
        final String content = mvcResult.getResponse().getContentAsString();
        JSONObject contentObject = new JSONObject(content);
        String groupDenominationResponse = contentObject.get("denomination").toString();

//        Assert
        assertEquals(HttpStatus.CREATED.value(), status);
        assertEquals("Sunday Runners",groupDenomination);
        assertTrue(contentObject.has("denomination"));
        assertTrue(contentObject.has("description"));
        assertTrue(contentObject.has("dateOfCreation"));
        assertTrue(contentObject.has("ledger"));
        assertTrue(contentObject.has("_links"));
    }

    @Test
    @DisplayName("Test getMapping | Happy Path")
    void getAllTransactions() throws Exception {

//      Arrange
//        Person email to add to the URI
        final String personEmail = "paulo@gmail.com";
//        Group email to add to the URI
        final String groupDenomination = "Sunday Runners";

//        Arrange transaction
        String denominationCategory = "IRS";
        String type = "debit";
        String transactionDescription = "first test";
        double amount = 50;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "Company";
        String date = "2020-07-06";

//        Uri
        final String uri = "/persons/" + personEmail + "/groups/"+groupDenomination+"/ledgers/records/US030/";

//          Input Json
        NewGroupTransactionInfoDTO newGroupTransactionInfoDTO = new NewGroupTransactionInfoDTO(denominationCategory, type,
                transactionDescription, amount, denominationAccountDeb, denominationAccountCred, date);
        String inputJson = super.mapToJson(newGroupTransactionInfoDTO);

//        Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
                .andReturn();

//        Get the HTTP response
        final int status = mvcResult.getResponse().getStatus();

//        Assign the response content to a JSON Object
        final String content = mvcResult.getResponse().getContentAsString();
        JSONObject contentObject = new JSONObject(content);
        String message = contentObject.get("message").toString();

//        Assert
        assertEquals(HttpStatus.NOT_FOUND.value(), status);
        assertEquals("Category does not exist; it needs to be created",message);
    }
}
