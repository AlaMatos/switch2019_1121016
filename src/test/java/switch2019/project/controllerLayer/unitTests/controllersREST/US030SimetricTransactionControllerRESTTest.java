package switch2019.project.controllerLayer.unitTests.controllersREST;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import switch2019.project.applicationLayer.applicationServices.US030SimetricTransactionService;
import switch2019.project.controllerLayer.controllers.controllersCLI.US030SimetricTransactionController;
import switch2019.project.controllerLayer.controllers.controllersREST.US030SimetricTransactionControllerRest;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Ledger;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Address;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.dtoLayer.dtos.CreateGroupTransactionDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtos.NewGroupCategoryInfoDTO;
import switch2019.project.dtoLayer.dtos.NewGroupTransactionInfoDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupTransactionDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Ala Matos
 */
@RestController
public class US030SimetricTransactionControllerRESTTest extends AbstractTest {

    @Autowired
    private US030SimetricTransactionControllerRest us030ControllerREST;
    @Mock
    private US030SimetricTransactionService us030Service;

    //    Parameters of person
    private Person personPaulo;
    private String pauloEmail;
    private PersonID pauloPersonID;
    private Ledger ledger;
    private LedgerID ledgerID;

    //    Parameters of a admin
//        Rita
    private String ritaEmail;
    private PersonID ritaPersonID;
    private CategoryID salaryID;
    private AccountID bankAccountID;
    private AccountID companyID;

    //    Parameters of group sundayRunners
    private Group sundayRunners;
    private GroupID sundayRunnersID;
    private Ledger sundayRunnersLedger;
    private LedgerID sundayRunnersLedgerID;
    private CategoryID groupSalaryID;

    private AccountID groupCompanyID;
    private AccountID groupBankAccountID;

    private Transaction salaryJanuary;
    private Transaction firstGroupSalaryJanuary;
    private Transaction secondGroupSalaryJanuary;


    @BeforeEach
    public void init() {

//        Ledger
        ledger = Ledger.createLedger();
        ledgerID = ledger.getLedgerID();

//        Person Paulo

//        Address
        String portoStreet = "Rua Direita do Viso";
        String portoDoorNumber = "59";
        String portoPostCode = "4250 - 198";
        String portoCity = "Porto";
        String portoCountry = "Portugal";

        Address porto = Address.createAddress(portoStreet, portoDoorNumber, portoPostCode, portoCity, portoCountry);

//        Information to instantiate a Person Paulo
        pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdate = LocalDate.of(1993, 03, 15);
        String pauloBirthplace = "Vila Nova de Gaia";

        pauloPersonID = PersonID.createPersonID(pauloEmail);

        personPaulo = Person.createPersonWithoutParents(pauloEmail, pauloName, pauloBirthdate, pauloBirthplace, porto, ledgerID);

//        Category Salary
        String salaryDenomination = "Salary";
        salaryID = CategoryID.createCategoryID(salaryDenomination, pauloPersonID);
        personPaulo.addCategory(salaryID);

//        Account Company
        String companyDenomination = "Company";
        String companyDescription = "Company account";
        companyID = AccountID.createAccountID(companyDenomination, pauloPersonID);
        personPaulo.addAccount(companyID);

//        Account Bank Account
        String bankAccountDenomination = "Bank Account";
        String bankAccountDescription = "Personal bank account";
        bankAccountID = AccountID.createAccountID(bankAccountDenomination, pauloPersonID);
        personPaulo.addAccount(bankAccountID);

//        Salary January
        String credit = "credit";
        String salaryJanuaryDescription = "January salary";
        double salaryJanuaryAmount = 1500;
        LocalDate salaryJanuaryDate = LocalDate.of(2020, 1, 21);
        salaryJanuary = Transaction.createTransaction(salaryID, credit, salaryJanuaryDescription, salaryJanuaryAmount, salaryJanuaryDate, companyID, bankAccountID);

        ledger.addTransaction(salaryJanuary);

//        Rita
        ritaEmail = "rita@gmail.com";
        ritaPersonID = PersonID.createPersonID(ritaEmail);

//        Henrique
        String henriqueEmail = "henrique@gmail.com";
        PersonID henriquePersonID = PersonID.createPersonID(henriqueEmail);


        //Sunday Runners
        //Accounts ->            Company / Bank Account
        //Categories ->          Salary

//        Ledger
        sundayRunnersLedger = Ledger.createLedger();
        sundayRunnersLedgerID = sundayRunnersLedger.getLedgerID();

        LocalDate dateOfCreation = LocalDate.of(2020, 6, 1);
        String sundayRunnersDenomination = "Sunday Runners";
        String sundayRunnersDescription = "All members from Sunday Runners group";

//        Information to instantiate a Group Sunday Runners
        sundayRunners = Group.createGroupAsPersonInCharge(sundayRunnersDenomination, pauloPersonID, sundayRunnersDescription, dateOfCreation, sundayRunnersLedgerID);
        sundayRunnersID = GroupID.createGroupID(sundayRunnersDenomination);

//        Add members
        sundayRunners.addMember(ritaPersonID);
        sundayRunners.addMember(henriquePersonID);

//        Category Salary
        String groupSalaryDenomination = "Salary";
        groupSalaryID = CategoryID.createCategoryID(groupSalaryDenomination, sundayRunnersID);
        sundayRunners.addCategory(groupSalaryID);

//        Category Groceries
        String groupGroceriesDenomination = "Groceries";
        CategoryID groupGroceriesID = CategoryID.createCategoryID(groupGroceriesDenomination, sundayRunnersID);
        sundayRunners.addCategory(groupGroceriesID);

//        Account Company
        String groupCompanyDenomination = "Company";
        String groupCompanyDescription = "Company account";
        groupCompanyID = AccountID.createAccountID(groupCompanyDenomination, sundayRunnersID);
        sundayRunners.addAccount(companyID);

//        Account Bank Account
        String groupBankAccountDenomination = "Bank Account";
        String groupBankAccountDescription = "Personal bank account";
        groupBankAccountID = AccountID.createAccountID(groupBankAccountDenomination, sundayRunnersID);
        sundayRunners.addAccount(bankAccountID);

//        Transaction Salary January
        String firstGroupCredit = "credit";
        String firstGroupSalaryJanuaryDescription = "January salhary";
        double firstGroupSalaryJanuaryAmount = 1500;
        LocalDate firstGroupSalaryJanuaryDate = LocalDate.of(2020, 01, 21);
        firstGroupSalaryJanuary = Transaction.createTransaction(groupSalaryID, firstGroupCredit, firstGroupSalaryJanuaryDescription,
                firstGroupSalaryJanuaryAmount, firstGroupSalaryJanuaryDate, groupCompanyID, groupBankAccountID);

//        Transaction Groceries January
        String secondGroupCredit = "debit";
        String secondGroupSalaryJanuaryDescription = "January exphenses";
        double secondGroupSalaryJanuaryAmount = 150;
        LocalDate secondGroupSalaryJanuaryDate = LocalDate.of(2020, 01, 29);
        secondGroupSalaryJanuary = Transaction.createTransaction(groupSalaryID, secondGroupCredit, secondGroupSalaryJanuaryDescription,
                secondGroupSalaryJanuaryAmount, secondGroupSalaryJanuaryDate, groupCompanyID, groupBankAccountID);

//          Add transactions to sunday Runners ledger
        sundayRunnersLedger.addTransaction(firstGroupSalaryJanuary);
        sundayRunnersLedger.addTransaction(secondGroupSalaryJanuary);
    }

    @Test
    @DisplayName("Test Controller REST | Happy path")
    void testControllerREST_HappyPath() {

//        Arrange
        String sundayRunnersDenomination = sundayRunnersID.getDenomination().getDenomination();

//          Create a list of groups
        List<Group> groupsList = new ArrayList<>();
        groupsList.add(sundayRunners);

//          Create a list of transactions for the group SundayRunners
        List<Transaction> transactionsList = new ArrayList<>();
        transactionsList.add(firstGroupSalaryJanuary);
        transactionsList.add(secondGroupSalaryJanuary);

//        Arrange transaction
        String denominationCategory = "Salary";
        String type = "debit";
        String transactionDescription = "first test";
        double amount = 50;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "Company";
        String date = "2020-07-06";

//          Create a SearchTransactionDTO
        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(sundayRunnersDenomination,
                pauloEmail, salaryID.getDenomination().getDenomination(), bankAccountID.getDenomination().getDenomination(), companyID.getDenomination().getDenomination(),
                amount, type, transactionDescription, date);

//          Get Fontes Family denomination
        Denomination denominationSundayRunners = sundayRunners.getGroupID().getDenomination();
        Description sundayRunnersDescription = sundayRunners.getDescription();
        DateOfCreation sundayRunnersDateOfCreation = sundayRunners.getDateOfCreation();

        GroupDTO expectedGroupFromDomain = GroupDTOAssembler.createDTOFromDomainObject(denominationSundayRunners, sundayRunnersDescription, sundayRunnersDateOfCreation);


//       Mock the behaviour of the service's getAllPersonsTransactions method
        Mockito.when(us030Service.createSimetricMovements(createGroupTransactionDTO))
                .thenReturn(expectedGroupFromDomain);

        NewGroupTransactionInfoDTO info = new NewGroupTransactionInfoDTO();
        info.setDenominationCategory(denominationCategory);
        info.setType(type);
        info.setDescription(transactionDescription);
        info.setAmount(amount);
        info.setDenominationAccountDeb(denominationAccountDeb);
        info.setDenominationAccountCred(denominationAccountCred);
        info.setDate(date);

        ResponseEntity<Object> expectedGroup = new ResponseEntity<>(expectedGroupFromDomain, HttpStatus.CREATED);

//        Create a SearchTransactionFromDomainDTO using the controller
        US030SimetricTransactionControllerRest us030SimetricTransactionControllerRest = new US030SimetricTransactionControllerRest(us030Service);
        ResponseEntity<Object> actualGroupDTO = us030SimetricTransactionControllerRest.simetricTransaction(info, pauloEmail, sundayRunnersDenomination);


//        Assert
        assertEquals(expectedGroup, actualGroupDTO);
    }

    @Test
    @DisplayName("Test the Controller REST | Person Not in charge")
    void testControllerRESTPersonNotInCharge() {
//        Arrange
        String sundayRunnersDenomination = sundayRunnersID.getDenomination().getDenomination();

//        Arrange transaction
        String denominationCategory = "Salary";
        String type = "debit";
        String transactionDescription = "first test";
        double amount = 50;
        String denominationAccountDeb = "Bank Account";
        String denominationAccountCred = "Company";
        String date = "2020-07-06";

//          Create a SearchTransactionDTO
        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(sundayRunnersDenomination,
                ritaEmail, salaryID.getDenomination().getDenomination(), bankAccountID.getDenomination().getDenomination(), companyID.getDenomination().getDenomination(),
                amount, type, transactionDescription, date);

//       Mock the behaviour of the service's getAllPersonsTransactions method
        Mockito.when(us030Service.createSimetricMovements(createGroupTransactionDTO))
                .thenThrow(new InvalidArgumentsBusinessException("Person is not in charge"));

        NewGroupTransactionInfoDTO info = new NewGroupTransactionInfoDTO();
        info.setDenominationCategory(denominationCategory);
        info.setType(type);
        info.setDescription(transactionDescription);
        info.setAmount(amount);
        info.setDenominationAccountDeb(denominationAccountDeb);
        info.setDenominationAccountCred(denominationAccountCred);
        info.setDate(date);

//        Create a SearchTransactionFromDomainDTO using the controller
        US030SimetricTransactionControllerRest us030SimetricTransactionControllerRest = new US030SimetricTransactionControllerRest(us030Service);

        //          Assign the return of the us019Service
        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us030SimetricTransactionControllerRest.simetricTransaction(info,
                ritaEmail, sundayRunnersDenomination));

//        Expected message
        String expectedMessage = "Person is not in charge";

//        Assert
        assertEquals(expectedMessage, thrown.getMessage());
    }
}



