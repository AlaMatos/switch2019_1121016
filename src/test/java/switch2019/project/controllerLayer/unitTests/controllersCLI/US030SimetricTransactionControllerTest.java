package switch2019.project.controllerLayer.unitTests.controllersCLI;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import switch2019.project.applicationLayer.applicationServices.US030SimetricTransactionService;
import switch2019.project.controllerLayer.controllers.controllersCLI.US019GetPersonAndMyGroupsTransactionsWithinAPeriodController;
import switch2019.project.controllerLayer.controllers.controllersCLI.US030SimetricTransactionController;
import switch2019.project.controllerLayer.integrationTests.AbstractTest;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Ledger;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Address;
import switch2019.project.domainLayer.domainEntities.aggregates.person.Person;
import switch2019.project.domainLayer.domainEntities.vosShared.*;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.*;
import switch2019.project.dtoLayer.dtos.CreateGroupTransactionDTO;
import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtos.SearchTransactionFromDomainDTO;
import switch2019.project.dtoLayer.dtosAssemblers.CreateGroupTransactionDTOAssembler;
import switch2019.project.dtoLayer.dtosAssemblers.GroupDTOAssembler;
import switch2019.project.utils.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Ala Matos
 */
public class US030SimetricTransactionControllerTest extends AbstractTest {

    @Mock
    private US030SimetricTransactionService us030Service;

    //    Parameters of person
    private Person personPaulo;
    private String pauloEmail;
    private PersonID pauloPersonID;
    private Ledger ledger;
    private LedgerID ledgerID;

    //    Parameters of a admin
//        Rita
    private String ritaEmail;
    private PersonID ritaPersonID;
    private CategoryID salaryID;
    private AccountID bankAccountID;
    private AccountID companyID;

    //    Parameters of group sundayRunners
    private Group sundayRunners;
    private GroupID sundayRunnersID;
    private Ledger sundayRunnersLedger;
    private LedgerID sundayRunnersLedgerID;
    private CategoryID groupSalaryID;

    private AccountID groupCompanyID;
    private AccountID groupBankAccountID;

    //    Parameters of group Fontes Family
    private Group fontesFamily;
    private GroupID fontesFamilyID;
    private Ledger fontesFamilyLedger;
    private LedgerID fontesFamilyLedgerID;

    private Transaction salaryJanuary;
    private Transaction firstGroupSalaryJanuary;
    private Transaction secondGroupSalaryJanuary;
    private Transaction thirdGroupSalaryJanuary;
    private Transaction fourthGroupGroceriesJanuary;


    @BeforeEach
    public void init() {

//        Ledger
        ledger = Ledger.createLedger();
        ledgerID = ledger.getLedgerID();

//        Person Paulo

//        Address
        String portoStreet = "Rua Direita do Viso";
        String portoDoorNumber = "59";
        String portoPostCode = "4250 - 198";
        String portoCity = "Porto";
        String portoCountry = "Portugal";

        Address porto = Address.createAddress(portoStreet, portoDoorNumber, portoPostCode, portoCity, portoCountry);

//        Information to instantiate a Person Paulo
        pauloEmail = "paulo@gmail.com";
        String pauloName = "Paulo Fontes";
        LocalDate pauloBirthdate = LocalDate.of(1993, 03, 15);
        String pauloBirthplace = "Vila Nova de Gaia";

        pauloPersonID = PersonID.createPersonID(pauloEmail);

        personPaulo = Person.createPersonWithoutParents(pauloEmail, pauloName, pauloBirthdate, pauloBirthplace, porto, ledgerID);

//        Category Salary
        String salaryDenomination = "Salary";
        salaryID = CategoryID.createCategoryID(salaryDenomination, pauloPersonID);
        personPaulo.addCategory(salaryID);

//        Account Company
        String companyDenomination = "Company";
        String companyDescription = "Company account";
        companyID = AccountID.createAccountID(companyDenomination, pauloPersonID);
        personPaulo.addAccount(companyID);

//        Account Bank Account
        String bankAccountDenomination = "Bank Account";
        String bankAccountDescription = "Personal bank account";
        bankAccountID = AccountID.createAccountID(bankAccountDenomination, pauloPersonID);
        personPaulo.addAccount(bankAccountID);

//        Salary January
        String credit = "credit";
        String salaryJanuaryDescription = "January salary";
        double salaryJanuaryAmount = 1500;
        LocalDate salaryJanuaryDate = LocalDate.of(2020, 1, 21);
        salaryJanuary = Transaction.createTransaction(salaryID, credit, salaryJanuaryDescription, salaryJanuaryAmount, salaryJanuaryDate, companyID, bankAccountID);

        ledger.addTransaction(salaryJanuary);

//        Rita
        ritaEmail = "rita@gmail.com";
        ritaPersonID = PersonID.createPersonID(ritaEmail);

//        Henrique
        String henriqueEmail = "henrique@gmail.com";
        PersonID henriquePersonID = PersonID.createPersonID(henriqueEmail);


        //Sunday Runners
        //Accounts ->            Company / Bank Account
        //Categories ->          Salary

//        Ledger
        sundayRunnersLedger = Ledger.createLedger();
        sundayRunnersLedgerID = sundayRunnersLedger.getLedgerID();

        LocalDate dateOfCreation = LocalDate.of(2020, 6, 1);
        String sundayRunnersDenomination = "Sunday Runners";
        String sundayRunnersDescription = "All members from Sunday Runners group";

//        Information to instantiate a Group Sunday Runners
        sundayRunners = Group.createGroupAsPersonInCharge(sundayRunnersDenomination, pauloPersonID, sundayRunnersDescription, dateOfCreation, sundayRunnersLedgerID);
        sundayRunnersID = GroupID.createGroupID(sundayRunnersDenomination);

//        Add members
        sundayRunners.addMember(ritaPersonID);
        sundayRunners.addMember(henriquePersonID);

//        Category Salary
        String groupSalaryDenomination = "Salary";
        groupSalaryID = CategoryID.createCategoryID(groupSalaryDenomination, sundayRunnersID);
        sundayRunners.addCategory(groupSalaryID);

//        Category Groceries
        String groupGroceriesDenomination = "Groceries";
        CategoryID groupGroceriesID = CategoryID.createCategoryID(groupGroceriesDenomination, sundayRunnersID);
        sundayRunners.addCategory(groupGroceriesID);

//        Account Company
        String groupCompanyDenomination = "Company";
        String groupCompanyDescription = "Company account";
        groupCompanyID = AccountID.createAccountID(groupCompanyDenomination, sundayRunnersID);
        sundayRunners.addAccount(companyID);

//        Account Bank Account
        String groupBankAccountDenomination = "Bank Account";
        String groupBankAccountDescription = "Personal bank account";
        groupBankAccountID = AccountID.createAccountID(groupBankAccountDenomination, sundayRunnersID);
        sundayRunners.addAccount(bankAccountID);

//        Transaction Salary January
        String firstGroupCredit = "credit";
        String firstGroupSalaryJanuaryDescription = "January salhary";
        double firstGroupSalaryJanuaryAmount = 1500;
        LocalDate firstGroupSalaryJanuaryDate = LocalDate.of(2020, 01, 21);
        firstGroupSalaryJanuary = Transaction.createTransaction(groupSalaryID, firstGroupCredit, firstGroupSalaryJanuaryDescription,
                firstGroupSalaryJanuaryAmount, firstGroupSalaryJanuaryDate, groupCompanyID, groupBankAccountID);

//        Transaction Groceries January
        String secondGroupCredit = "debit";
        String secondGroupSalaryJanuaryDescription = "January exphenses";
        double secondGroupSalaryJanuaryAmount = 150;
        LocalDate secondGroupSalaryJanuaryDate = LocalDate.of(2020, 01, 29);
        secondGroupSalaryJanuary = Transaction.createTransaction(groupSalaryID, secondGroupCredit, secondGroupSalaryJanuaryDescription,
                secondGroupSalaryJanuaryAmount, secondGroupSalaryJanuaryDate, groupCompanyID, groupBankAccountID);

//          Add transactions to sunday Runners ledger
        sundayRunnersLedger.addTransaction(firstGroupSalaryJanuary);
        sundayRunnersLedger.addTransaction(secondGroupSalaryJanuary);

        //Fontes Family
        //Accounts ->            Company / Bank Account
        //Categories ->          Salary

//        Ledger
        fontesFamilyLedger = Ledger.createLedger();
        fontesFamilyLedgerID = fontesFamilyLedger.getLedgerID();

        LocalDate fontesFamilyDateOfCreation = LocalDate.of(2020, 6, 2);
        String fontesFamilyDenomination = "Fontes Family";
        String fontesFamilyDescription = "All members from Fontes Family";

//        Information to instantiate a Group Sunday Runners
        fontesFamily = Group.createGroupAsPersonInCharge(fontesFamilyDenomination, ritaPersonID, fontesFamilyDescription, fontesFamilyDateOfCreation, fontesFamilyLedgerID);
        fontesFamilyID = GroupID.createGroupID(fontesFamilyDenomination);

//        Add members
        fontesFamily.addMember(pauloPersonID);
        fontesFamily.addMember(henriquePersonID);

        //        Transaction Salary January
        String thirdGroupCredit = "credit";
        String thirdGroupSalaryJanuaryDescription = "January salary";
        double thirdGroupSalaryJanuaryAmount = 1500;
        LocalDate thirdGroupSalaryJanuaryDate = LocalDate.of(2020, 01, 21);
        thirdGroupSalaryJanuary = Transaction.createTransaction(groupSalaryID, thirdGroupCredit, thirdGroupSalaryJanuaryDescription,
                thirdGroupSalaryJanuaryAmount, thirdGroupSalaryJanuaryDate, groupCompanyID, groupBankAccountID);

//        Transaction Groceries January
        String fourthGroupCredit = "debit";
        String fourthGroupSalaryJanuaryDescription = "January expenses";
        double fourthGroupSalaryJanuaryAmount = 150;
        LocalDate fourthGroupSalaryJanuaryDate = LocalDate.of(2020, 01, 29);
        fourthGroupGroceriesJanuary = Transaction.createTransaction(groupSalaryID, fourthGroupCredit, fourthGroupSalaryJanuaryDescription,
                fourthGroupSalaryJanuaryAmount, fourthGroupSalaryJanuaryDate, groupCompanyID, groupBankAccountID);

//          Add transactions to sunday Runners ledger
        fontesFamilyLedger.addTransaction(thirdGroupSalaryJanuary);
        fontesFamilyLedger.addTransaction(fourthGroupGroceriesJanuary);

        //        |--Transactions
//                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
//                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
//                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
//                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
//                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
//                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
//                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500

         /*
        Group -> Fontes Family
          |
          |--peopleInCharge ->      Manuel Fontes (manuel@gmail.com) / Ilda Fontes (ilda@gmail.com)
          |--members ->             Paulo Fontes (paulo@gmail.com) / Helder Fontes (helder@gmail.com)
          |--Accounts ->            Company / Bank Account / Wallet / State / Supermarket / Household Expenses / Streaming Services
          |--Categories ->          Salary / Draw Money / IRS / Food / Water Bill / Netflix
          |--Transactions
                |-> 2020-01-21 / CREDIT / Salary / Company -> Bank Account / 1500
                |-> 2020-01-25 / CREDIT / Draw Money / Bank Account -> Wallet / 100
                |-> 2020-01-27 / DEBIT / IRS / Bank Account -> State / 150
                |-> 2020-01-29 / DEBIT / Food / Wallet -> Supermaket / 50
                |-> 2020-01-30 / DEBIT / Water Bill / Bank Account -> Household Expenses / 50
                |-> 2020-01-30 / DEBIT / Netflix / Bank Account -> Streaming Services / 25
                |-> 2020-02-21 / CREDIT / Salary / Company -> Bank Account / 1500
         */
    }

    @Test
    @DisplayName("Test the service | Simetric transaction between personledger and group ledger")
    void testServiceOnePersonOneGroup() {

//        Arrange
        String sundayRunnersDenomination = sundayRunnersID.getDenomination().getDenomination();


//          Create a list of groups
        List<Group> groupsList = new ArrayList<>();
        groupsList.add(sundayRunners);

//          Create a list of transactions for the group SundayRunners
        List<Transaction> transactionsList = new ArrayList<>();
        transactionsList.add(firstGroupSalaryJanuary);
        transactionsList.add(secondGroupSalaryJanuary);

//        Arrange transaction
        double firstTransactionAmount = 54.00;
        String firstTransactionType = "credit";
        String firstTransactionDescription = "Dividends";
        String firstTransactionDate = "2020-06-17";

//          Create a SearchTransactionDTO
        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(sundayRunnersDenomination,
                pauloEmail, salaryID.getDenomination().getDenomination(), bankAccountID.getDenomination().getDenomination(), companyID.getDenomination().getDenomination(),
                firstTransactionAmount, firstTransactionType, firstTransactionDescription, firstTransactionDate);

//          Get Fontes Family denomination
        Denomination denominationSundayRunners = sundayRunners.getGroupID().getDenomination();

        Description sundayRunnersDescription = sundayRunners.getDescription();
        DateOfCreation sundayRunnersDateOfCreation = sundayRunners.getDateOfCreation();

        GroupDTO expectedGroup = GroupDTOAssembler.createDTOFromDomainObject(denominationSundayRunners, sundayRunnersDescription, sundayRunnersDateOfCreation);


//       Mock the behaviour of the service's getAllPersonsTransactions method
        Mockito.when(us030Service.createSimetricMovements(createGroupTransactionDTO))
                .thenReturn(expectedGroup);

//        Create a SearchTransactionFromDomainDTO using the controller
        US030SimetricTransactionController us030SimetricTransactionController = new US030SimetricTransactionController(us030Service);
        GroupDTO actualGroupDTO = us030SimetricTransactionController.createGroupTransaction(sundayRunnersDenomination,
                pauloEmail, salaryID.getDenomination().getDenomination(), bankAccountID.getDenomination().getDenomination(), companyID.getDenomination().getDenomination(),
                firstTransactionAmount, firstTransactionType, firstTransactionDescription, firstTransactionDate);

//        Assert
        assertEquals(expectedGroup, actualGroupDTO);
    }

    @Test
    @DisplayName("Test the service | Transactions from 1 person and 2 group")
    void testServiceOnePersonTwoGroup() {
//        Arrange
        String sundayRunnersDenomination = sundayRunnersID.getDenomination().getDenomination();


        //          Create a list of groups
        List<Group> groupsList = new ArrayList<>();
        groupsList.add(sundayRunners);

        //          Create a list of transactions for the group SundayRunners
        List<Transaction> transactionsList = new ArrayList<>();
        transactionsList.add(firstGroupSalaryJanuary);
        transactionsList.add(secondGroupSalaryJanuary);

        //        Arrange transaction
        double firstTransactionAmount = 54.00;
        String firstTransactionType = "credit";
        String firstTransactionDescription = "Dividends";
        String firstTransactionDate = "2020-06-17";

        //          Create a SearchTransactionDTO
        CreateGroupTransactionDTO createGroupTransactionDTO = CreateGroupTransactionDTOAssembler.createDTOFromPrimitiveTypes(sundayRunnersDenomination,
                ritaEmail, salaryID.getDenomination().getDenomination(), bankAccountID.getDenomination().getDenomination(), companyID.getDenomination().getDenomination(),
                firstTransactionAmount, firstTransactionType, firstTransactionDescription, firstTransactionDate);

        //          Get Sunday Runners denomination
        Denomination denominationSundayRunners = sundayRunners.getGroupID().getDenomination();

        Description sundayRunnersDescription = sundayRunners.getDescription();
        DateOfCreation sundayRunnersDateOfCreation = sundayRunners.getDateOfCreation();

        GroupDTO expectedGroup = GroupDTOAssembler.createDTOFromDomainObject(denominationSundayRunners, sundayRunnersDescription, sundayRunnersDateOfCreation);


//       Mock the behaviour of the service's getAllPersonsTransactions method
        Mockito.when(us030Service.createSimetricMovements(createGroupTransactionDTO))
                .thenThrow(new InvalidArgumentsBusinessException("Person is not in charge"));

        //        Create a SearchTransactionFromDomainDTO using the controller
        US030SimetricTransactionController us030SimetricTransactionController = new US030SimetricTransactionController(us030Service);

//        GroupDTO actualGroupDTO = us030SimetricTransactionController.createGroupTransaction(sundayRunnersDenomination,
//                pauloEmail, salaryID.getDenomination().getDenomination(), bankAccountID.getDenomination().getDenomination(), companyID.getDenomination().getDenomination(),
//                firstTransactionAmount, firstTransactionType, firstTransactionDescription, firstTransactionDate);

        Throwable thrown = assertThrows(InvalidArgumentsBusinessException.class, () -> us030SimetricTransactionController.createGroupTransaction(sundayRunnersDenomination,
                ritaEmail, salaryID.getDenomination().getDenomination(), bankAccountID.getDenomination().getDenomination(), companyID.getDenomination().getDenomination(),
                firstTransactionAmount, firstTransactionType, firstTransactionDescription, firstTransactionDate));

        String message = thrown.getMessage();
        String expectedMessage = "Person is not in charge";
        //        Assert
        assertEquals(expectedMessage, message);
    }
//
//    @Test
//    @DisplayName("Test the service | Without Start Date")
//    void testServiceWithoutStartDate() {
//
////        Arrange
//        us019Service = new US019GetPersonAndMyGroupsTransactionsWithinAPeriodService(personRepository, groupRepository,
//                ledgerRepository, getListGroupTransactions, getPersonsGroups, assemblePersonID);
//
////          Returns an Optional<Person> Paulo
//        Mockito.when(personRepository.findById(pauloPersonID)).thenReturn(Optional.of(personPaulo));
//
////          Returns an Optional<Ledger>
//        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(ledger));
//
////          Create a list of groups
//        List<Group> groupsList = new ArrayList<>();
//        groupsList.add(sundayRunners);
//
////          Returns an Optional<Group>
//        Mockito.when(groupRepository.findById(sundayRunnersID)).thenReturn(Optional.of(sundayRunners));
//
////          Returns the list of all groups
//        Mockito.when(groupRepository.findAll()).thenReturn(groupsList);
//
////          Returns an Optional<Ledger>
//        Mockito.when(ledgerRepository.findById(sundayRunnersLedgerID)).thenReturn(Optional.of(sundayRunnersLedger));
//
////          Define the start and end date
//        String startDate = "";
//        String endDate = "2020-02-12";
//
////          Create a list of transactions for the group SundayRunners
//        List<Transaction> transactionsList = new ArrayList<>();
//        transactionsList.add(firstGroupSalaryJanuary);
//        transactionsList.add(secondGroupSalaryJanuary);
//
////          Create a SearchTransactionDTO
//        SearchTransactionDTO dataToSearch = new SearchTransactionDTO(pauloEmail, startDate, endDate);
//
////          Returns the list of groups of the person
//        Mockito.when(getPersonsGroups.getPersonsGroups(dataToSearch)).thenReturn(groupsList);
//
//        PersonID pauloID = PersonID.createPersonID(pauloEmail);
//
////          Returns the personID of the person to analyze
//        Mockito.when(assemblePersonID.assemblePersonID(dataToSearch.getPersonID())).thenReturn(pauloID);
//
//        //Act
////          Assign the return of the us019Service
//        Throwable thrown = assertThrows(NotFoundArgumentsBusinessException.class, () -> us019Service.getAllPersonsTransactions(dataToSearch));
//
////        Expected message
//        String expectedMessage = "Search results cannot be displayed: start date is missing";
//
////        Assert
//        assertEquals(expectedMessage, thrown.getMessage());
    }

//    }


