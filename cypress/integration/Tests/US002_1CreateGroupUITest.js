/// <reference types="cypress" />
import {Login} from "../../../src/js/links/Login";

describe('Test the US002_1CreateGroup', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })

    it('Test the US002.1 UI | Happy path', () => {

        //Add a page resolution
        cy.viewport(1920, 1080)

            //Log into the app
            .get(':nth-child(3) > .form-control')

            //Type the username
            .type('paulo@gmail.com', {delay: 100})

            //Click on the login button
            .get('.btn').click()

            //Click on the MyGroups NavBar link
            .get('.navbar-nav > :nth-child(2) > a').click()

            //Add a group with the denomination Switch2019
            .get('[placeholder="Please enter denomination"]').type("Switch2019", {delay: 100})
            .get('[placeholder="Please enter description"]').type("Switch2019", {delay: 100})
            .get('.Forms > .btn').click()
    })
})

