/// <reference types="cypress" />
import {Login} from "../../../src/js/links/Login";

describe('Test the US003AddPersonToGroupUI', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })

    it('Test the US003 UI | Happy path', () => {

        //Add a page resolution
        cy.viewport(1920, 1080)

            //Log into the app
            .get(':nth-child(3) > .form-control')

            //Type the username
            .type('paulo@gmail.com', {delay: 100})

            //Click on the login button
            .get('.btn').click()

            //Click on the MyGroups NavBar link
            .get('.navbar-nav > :nth-child(2) > a').click()

            //Select the first group of the list  (in this case is Fontes Family)
            .get(':nth-child(1) > .GroupDenomination').click()

            //Add the person (henrique@gmail.com) to the selected group
            .get('.homeNavBar > :nth-child(1) > a').click()
            .get('input').type("henrique@gmail.com", {delay: 100})
            .get('.Forms > .btn').click()
    })
})