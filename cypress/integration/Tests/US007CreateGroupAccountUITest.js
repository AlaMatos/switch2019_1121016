/// <reference types="cypress" />
import {Login} from "../../../src/js/links/Login";

describe('Test the US006CreatePersonAccount', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })
    it('Test the US007 UI | Happy path', () => {

        //Add a page resolution
        cy.viewport(1920, 1080)
            //Log into the app
            .get(':nth-child(3) > .form-control')

            //Type the username
            .type('paulo@gmail.com', {delay: 100})

            //Click on the login button
            .get('.btn').click()

            //Click ont the MyGroups NavBar link
            .get('.navbar-nav > :nth-child(2) > a').click()

            //Select the second group of the list (in this case is Sunday Runners)
            .get(':nth-child(2) > .GroupDenomination').click()

            //Select the accounts tab
            .get(':nth-child(3) > a').click()

            //Add an account with the denomination Groceries
            .get('[placeholder="Please enter denomination"]').type("Groceries")
            .get('[placeholder="Please enter description"]').type("July groceries")
            .get('.Forms > .btn').click()
    })

    it('Test the US007 UI | The user do not have permissions to add Account to the group', () => {

        //Add a page resolution
        cy.viewport(1920, 1080)
            //Log into the app
            .get(':nth-child(3) > .form-control')

            //Type the username
            .type('paulo@gmail.com', {delay: 100})

            //Click on the login button
            .get('.btn').click()

            //Click ont the MyGroups NavBar link
            .get('.navbar-nav > :nth-child(2) > a').click()

            //Select the first group of the list (in this case is Fontes Family)
            .get(':nth-child(1) > .GroupDenomination').click()

            //Select the accounts tab
            .get(':nth-child(3) > a').click()

            //Add an account with the denomination Groceries
            .get('[placeholder="Please enter denomination"]').type("Groceries")
            .get('[placeholder="Please enter description"]').type("July groceries")
            .get('.Forms > .btn').click()
    })
})